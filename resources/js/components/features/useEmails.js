import { reactive, toRefs } from "vue";

const state = reactive({
  emails: [],
  showing: null,
  search_result: [],
  api_token: ''
})

const saveToken = (token) => {
  state.api_token = token
}

const getToken = () => {
  return state
}

const updateShowing = (id) => {
  state.showing = id
}

const updateSearchResult = (search_term, search_by) => {
  const search_fields = {
    recipient: 'recipient',
    subject: 'subject',
    content: 'content'
  }

  state.search_result = [];

  let exp = new RegExp(search_term, 'gi');

  if (search_by === 'all') {
    state.emails.map(email => {
      if (email.recipient.search(exp) >= 0 || email.subject.search(exp) >= 0 || email.content.search(exp) >= 0) {
        state.search_result.push(email);
      }
    })
  } else {
    state.emails.map(email => {
      if (email[search_fields[search_by]].search(exp) >= 0) {
        state.search_result.push(email);
      }
    })
  }
}

const clearSearchResult = () => {
  state.search_result = []
}

const getSentEmails = () => {
  axios
    .get(`/emails/${state.api_token}`)
    .then((response) => {
      if (response.status === 200 && response.data.success) {
        state.emails = JSON.parse(response.data.emails);
      }
    })
    .catch((err) => {
      // console.log(err);
    });
};

const abortSendingEmail = (email_id) => {
  axios
    .post(`/emails/abort/${email_id}/${state.api_token}`)
    .then((response) => {
      if (response.status === 200 && response.data.success) {
        getSentEmails()
      }
    })
    .catch((err) => {
      // console.log(err);
    });
}

export default {
  ...toRefs(state),
  getToken,
  saveToken,
  getSentEmails,
  updateShowing,
  clearSearchResult,
  abortSendingEmail,
  updateSearchResult,
}
