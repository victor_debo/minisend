require('./bootstrap');

import { createApp, defineAsyncComponent } from 'vue';


const DashboardComponent = defineAsyncComponent(() => import(
  /* webpackChunkName: "dashboard-component" */
  './components/DashboardComponent.vue'
));

const LoginSignupComponent = defineAsyncComponent(() => import(
  /* webpackChunkName: "login-signup-component" */
  './components/LoginSignupComponent.vue'
));

const ClientsComponent = defineAsyncComponent(() => import(
  /* webpackChunkName: "clients-component" */
  './components/ClientsComponent.vue'
));

const app = createApp({})

createApp({
  components: {
    ClientsComponent,
    DashboardComponent,
    LoginSignupComponent,
  }
})


app.component('clients-component', ClientsComponent);
app.component('dashboard-component', DashboardComponent);
app.component('login-signup-component', LoginSignupComponent);

app.mount('#app')

