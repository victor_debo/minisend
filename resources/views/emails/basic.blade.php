<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>{{ $subject }}</title>
</head>
<body>
    @if($content_type === 'text')
        <p class="text-left"> {{ $content }} </p>
    @else
        <p class="text-left tracking-normal"> {!! $content !!} </p>
    @endif

</body>
</html>
