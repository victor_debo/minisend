@extends('layouts.app')

@section('content')

<!-- This example requires Tailwind CSS v2.0+ -->
<div>
    @include('layouts.navbar')

  <header class="bg-white shadow">
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
      <h1 class="text-3xl font-bold leading-tight text-gray-900">
        Dashboard
      </h1>
    </div>
  </header>
  <main>
    <div class="max-w-7xl mx-auto py-6 sm:px-6 lg:px-8">
      <!-- Replace with your content -->
      <div id="app">
          <dashboard-component token="{{ Auth::user()->api_token }}"></dashboard-component>
      </div>
      <!-- /End replace -->
    </div>
  </main>
</div>

@endsection

@section('js')
    <script src="{{ asset('js/minisend.js') }}"></script>
    <script src="{{ asset('js/dashboard-component.js') }}"></script>
@endsection
