<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\TransactionEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function showDashboard()
    {
        return view('dashboard');
    }

    public function showClients()
    {
        return view('clients');
    }

    /**
     * clents they have sent mails to
     *
     * @param  mixed $request
     * @return void
     */
    public function getClientsRecipients(Request $request, $token)
    {
        if(!$this->_isAuthorised($token)) return $this->_sendUnAuthorisedResponse();

        $clients_list = Auth::user()->sentEmails()->get()->unique('recipient');

        return response(['clients' => $clients_list]);
    }

    /**
     * get emails sent to client($user_id)
     *
     * @param  mixed $request
     * @param  mixed $email_id
     * @param  mixed $token
     * @return void
     */
    public function getRecepientsEmails(Request $request, $email, $token)
    {
        if(!$this->_isAuthorised($token)) return $this->_sendUnAuthorisedResponse();

        $emails = TransactionEmail::where(['user_id' => Auth::user()->id, 'recipient' => $email])->get();

        return response($emails);
    }

    public function getSentEmails(Request $request, $api_token)
    {
        if(!$this->_isAuthorised($api_token)) return $this->_sendUnAuthorisedResponse();

        $emails = Auth::user()->sentEmails;

        if($emails->count() == 0) {
            return response('No emails sent yet');
        }

        return $this->_returnSuccessResponse(['emails' => $emails->toJson()]);
    }

    public function abortSendingEmail(Request $request, $id, $api_token)
    {
        return $this->_returnSuccessResponse();

        if(!$this->_isAuthorised($api_token)) return $this->_sendUnAuthorisedResponse();

        $email = TransactionEmail::find($id);

        if($email && $email->email_can_be_aborted){
            $email->cancelled = 1;
            $email->save();

            return $this->_returnSuccessResponse();
        }

        return response([
            'success' => false,
            'msg' => 'Unable to abort',
        ], 409);
    }

    private function _isAuthorised($api_token)
    {
        return (Auth::user() && Auth::user()->is(User::where('api_token', $api_token)->first())) ? true : false;
    }

    private function _sendUnAuthorisedResponse()
    {
        return response()->json([
            'success' => false,
            'msg' => 'Unauthorised access'
        ], 401);
    }

    private function _returnSuccessResponse($params = [])
    {
          return response(
            array_merge($params, ['success' => true])
        );
    }
}
