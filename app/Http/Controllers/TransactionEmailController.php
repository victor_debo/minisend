<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\EmailTemplate;
use App\Models\TransactionEmail;
use App\Models\EmailAttachment;
use App\Jobs\ProcessTransactionEmail;



class TransactionEmailController extends Controller
{
    public function sendMail(Request $request, $api_token)
    {

        $user = User::where('api_token', $api_token)->first();
        if(is_null($user)) return $this->_returnInvalidTokenResponse();

        $email_content_type = 'text';
        if($request->content !== strip_tags($request->content)) $email_content_type = 'html';

        $files = $this->_saveAttachments($request);

        $template_name = $request->template_name ?? 'BasicMail';

        $mail = TransactionEmail::create([
            'status' => '0',
            'user_id' => $user->id,
            'recipient' => $request->to,
            'subject' => $request->subject,
            'content' => $request->content,
            'content_type' => $email_content_type,
            'template_name' => ucfirst($template_name).'Mail'
        ]);

        if($mail && !empty($files)){
            EmailAttachment::create([
                'email_id' => $mail->id,
                'files' => json_encode($files)
            ]);
        }

        // ProcessTransactionEmail::dispatchSync($mail);
        ProcessTransactionEmail::dispatch($mail)->delay(now()->addMinutes(5));

        return response()->json([
            'success' => true,
            'msg' => 'Processing request'
        ], 102);
    }

    private function _returnInvalidTokenResponse()
    {
        return response()->json([
            'success' => false,
            'msg' => 'Invalid token'
        ], 401);
    }

    private function _saveAttachments(Request $request)
    {
        $file_paths = [];

        if($request->file('attachment')){
            foreach($request->file('attachment') as $idx => $file){
                array_push($file_paths, $request->file('attachment')[$idx]->store('attachment'));
            }
        }

        return $file_paths;
    }
}
