<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login-register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->only(['username', 'email', 'password']), [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 401);
        }

        $api_token = $this->_createToken($request->email);

        Auth::login($user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'api_token' => $api_token,
            'password' => Hash::make($request->password),
        ]));

        if($user){
            return response()->json([
                'success' => true,
                'msg' => 'Signup successfull'
            ]);
        }

        return response()->json([
            'success' => false,
            'msg' => 'Signup unsuccessfull'
        ]);
    }

    public function _createToken($email)
    {
        $str =  (string) time() . $email;
        $try_token = md5($str);

        while(User::where('api_token', $try_token)->first()){
            $try_token =  md5((string) time() . $email);
        }

        return $try_token;
    }
}
