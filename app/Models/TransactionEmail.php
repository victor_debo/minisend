<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionEmail extends Model
{
    use HasFactory;

    protected $table = 'transaction_emails';

    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d',
    ];

     protected $hidden = [
        'error_reason',
        'template_name',
        'updated_at',
    ];

    public function attachment()
    {
        return $this->hasOne(EmailAttachment::class, 'email_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getEmailStatusAttribute()
    {
        if($this->cancelled === 1) return 'cancelled';
        if($this->status == 1) return 'sent';
        if($this->status == 0) return 'pending';
        if($this->status == -1) return 'failed';
    }

    public function getMailableAttribute()
    {
        $mailable = "App\\Mail\\".$this->template_name;
        return new $mailable($this);
    }

    public function getEmailCanBeAbortedAttribute()
    {
        return ($this->status == 0 && $this->cancelled != 1) ? true : false;
    }

    public function getHasBeenCancelledAttribute()
    {
        return $this->cancelled == 1 ? true : false;
    }

}
