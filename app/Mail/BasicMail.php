<?php

namespace App\Mail;

use App\Models\TransactionEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BasicMail extends Mailable
{
    use Queueable, SerializesModels;

    public $transaction_email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(TransactionEmail $transaction_email)
    {
        $this->transaction_email = $transaction_email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_obj = $this->from($this->transaction_email->sender->email)
                ->view('emails.basic');

        $mail_obj = $this->_attachFiles($mail_obj);

        $mail_obj->with([
            'subject' => $this->transaction_email->subject,
            'content' => $this->transaction_email->content,
            'content_type' => $this->transaction_email->content_type
        ]);

        $mail_obj->subject($this->transaction_email->subject);

        return $mail_obj;
    }

    public function _attachFiles($mail_obj)
    {
        if($this->transaction_email->attachment){
            $files = json_decode($this->transaction_email->attachment->files);
            foreach($files as $file_path){
                $mail_obj->attach(storage_path('app/'.$file_path));
            }
        }

        return $mail_obj;
    }
}
