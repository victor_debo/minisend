<?php

namespace App\Jobs;

use App\Models\TransactionEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ProcessTransactionEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    // use Dispatchable, InteractsWithQueue, Queueable;

    public $tries = 2;

    public $transactionEmail;

    public $maxExceptions = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TransactionEmail $transactionEmail)
    {
        $this->transactionEmail = $transactionEmail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if(!$this->transactionEmail->has_been_cancelled){

                Mail::to($this->transactionEmail->recipient)
                ->send($this->transactionEmail->mailable);

                $this->transactionEmail->status = '1';
                $this->transactionEmail->save();
            }

        } catch (\Throwable $th) {

            $this->transactionEmail->status = '-1';
            $this->transactionEmail->error_reason = json_encode([
                'reason' => $th->getMessage(),
                'on' => $th->getFile(),
                'at' => $th->getLine(),
            ]);

            $this->transactionEmail->save();
        }

    }
}
