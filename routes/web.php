<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login', 301);

Route::get('/dashboard/refresh_token', [DashboardController::class, 'refreshToken'])->middleware(['auth']);

Route::get('/dashboard', [DashboardController::class, 'showDashboard'])->middleware(['auth'])->name('dashboard');
Route::get('/dashboard/clients', [DashboardController::class, 'showClients'])->middleware(['auth']);

Route::get('/recipients/{token}', [DashboardController::class, 'getClientsRecipients'])->middleware(['auth']);
Route::get('/recipients/emails/{email}/{token}', [DashboardController::class, 'getRecepientsEmails'])->middleware(['auth']);

Route::get('/emails/{api_token}', [DashboardController::class, 'getSentEmails'])->middleware(['auth']);
Route::post('/emails/abort/{id}/{api_token}', [DashboardController::class, 'abortSendingEmail'])->middleware(['auth']);



require __DIR__.'/auth.php';
