<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrasanctionEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_emails', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->index();
            $table->string('recipient');
            $table->string('subject');
            $table->longText('content');
            $table->enum('status', ['-1','0','1'])->default(0)->comment("-1 = failed, 0 = pending, 1 = sent");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_emails');
    }
}
