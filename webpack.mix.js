const mix = require('laravel-mix');

mix.webpackConfig({
  output: {
    publicPath: '/',
    chunkFilename: 'js/[name].js',
  },
});

mix.webpackConfig(webpack => {
  return {
    plugins: [
      new webpack.DefinePlugin({
        __VUE_OPTIONS_API__: true,
        __VUE_PROD_DEVTOOLS__: true
      })
    ]
  };
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
  .js('resources/js/minisend.js', 'public/js')
  .vue();

mix.postCss('resources/css/app.css', 'public/css', [
  require('postcss-import'),
  require('tailwindcss'),
  require('autoprefixer'),
]);

