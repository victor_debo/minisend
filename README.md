## Requirements ##
A mini version of a transactional email app, 
where a client could send an email through a Laravel API and also would have the ability to 
see how many emails have been sent, and track more interesting data about them through a VueJS frontend.

## Email API ##
- Allow sending emails with these fields:
- From (Sender)
- To (Recipient)
- Subject
- Text content
- HTML content
- Attachment(-s)

## A frontend ##
- A list of email activities
- Search by sender, recipient, subject
- Possible statuses:
- Posted
- Sent
- Failed
- A view of single email
- A list of emails for a recipient

## How to use
After signup a user will have a an api key they can use to send emails via client like postman

